#ifndef CESCAPE_H
#define CESCAPE_H
#define FPS 12
#define GAME_SPEED 1000 / FPS
#define X_SCALE 2 /* Ideally, it's a 2:1 ratio since terminal characters are twice as tall as they are wide */
#define Y_SCALE 1

typedef enum {
    dir_none,
    dir_up,
    dir_right,
    dir_down,
    dir_left,
} direction;

typedef struct shield {
    int active;
    int duration;
    int age;
    int energy;
} shield;

typedef struct entity {
    int x;
    int y;
    int state;
    direction direction;
} entity; 

typedef struct player {
    int cur_room;
    int x;
    int y;
    int health;
    int paused;
    int last_key;
    int hud_active;
    direction direction;
    shield shield;
} player;

typedef struct room {
    int id;
    int width;
    int height;
    int force_1by1;
    const char *map;
    entity bird;
    entity food;
    void (*rules)();
} room;

void bird_logic(entity *bird, player *p);
void end_game(int exit_code);
void food_logic(entity *food, player *p);
void game_loop();
void player_do_keys(player *p);
void player_draw(room *r, player *p);
char *player_health_bar(player *p);
void player_move(player *p);
char *player_shield_bar(player *p);
void player_turn_around(player *p);
void room_draw(room *r);
#endif
