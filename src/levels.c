/* This file contains the levels designed by Perez, with some extra
 * decorations to make them look more "cave-y", plus an easter egg
 * I threw in (room 12)
 */

#include <ncurses.h>
#include <stdlib.h>

#include "cescape.h"
#include "levels.h"

const int num_rooms = 13;
int sel_menu = 0;

room rooms[] = {
    { // Menu
        0,      // Room ID
        30, 12, // Width, Height
        1,      // Force 1:1 ratio (true if 1, false if 0)
        "                              " /* map */
        "         Cave Escape          "
        "                              "
        "                              "
        "            Start             "
        "            Exit              "
        "                              "
        "                              "
        "    (Press Space to choose)   "
        "    (Press Enter to select)   "
        "                              "
        "                              ",
        {0, 0, 0, dir_none}, // Bird (x, y, animation state, direction)
        {0, 0, 0, dir_none}, // Food
        menu_loop            // controller ("rules") function
        /* See cescape.h for the definition of the "room" type */
    },
    { // Room 1
        1,
        30, 12,
        0,
        "OxxxxxOOxxxOxxxoxooooOxxxOxxxO"
        "  ^ |     |    ^     |    |   "
        "                              "
        "                              "
        "                              "
        "                              "
        "                              "
        "                              "
        "                              "
        "                              "
        "    | ,     ,   |       ,  |  "
        "xxxxxxOxxxOxxxxxOxxOxxOooxxxxx",
        {0, 0, 0, dir_down},
        {0, 0, 0, dir_none},
        room_1
    },
    { // Room 2
        2,
        30, 12,
        0,
        "oooxxxOOxxoooOOoxxxoOOxxxOoxxx"
        "o      ^      ^          ^    "
        "o                             "
        "x                             "
        "x                             "
        "o                             "
        "O                             "
        "o                             "
        "x                             "
        "x                             "
        "x          ,              ,   "
        "o          OxxxxxxoxxOxxOxxxxO",
        {0, 0, 0, dir_right},
        {0, 0, 0, dir_none},
        room_2
    },
    { // Room 3
        3,
        30, 12,
        0,
        "x          xxoOoxxOOooxooxoxox"
        "x          xOxxxOoxxxxxOOoxxxx"
        "o  .       .   .   .   . . .::"
        "o            .   .   .   . .::"
        "O      .   .   .   .   . . .::"
        "x            .   .   .   . .::"
        "x  .       .   .   .   . . .::"
        "o            .   .   .   . .::"
        "O      .   .   .   .   . . .::"
        "o            .   .   .   . .::"
        "x  .       .   .   .   . . .::"
        "xxOoxxOOOxxxxxOOxxxxOOxxxxOOxx",
        {0, 0, 0, dir_right},
        {0, 0, 0, dir_none},
        room_3
    },
    { // Room 4
        4,
        30, 12,
        0,
        "OxxxxxOOO   OxxxxOOOOxxxxxOOOO"
        "                             x"
        "                             o"
        "                             o"
        "                             x"
        "                             o"
        "                             O"
        "                             x"
        "                             o"
        "                             o"
        "                             x"
        "xxxxxxOxxxOxxxxxOxOxxOoooxxxxx",
        {0, 0, 0, dir_left},
        {0, 0, 0, dir_none},
        room_4
    },
    { // Room 5
        5,
        30, 12,
        0,
        "        0   0                 "
        "        0   0                 "
        "        0   0                 "
        "        0   0                 "
        "        0   0                 "
        "        0   0                 "
        "        0   0                 "
        "        0   0                 "
        "        0   0                 "
        "        0   0                 "
        "        0   0                 "
        "        0   0                 ",
        {0, 0, 0, dir_down},
        {0, 0, 0, dir_none},
        room_5
    },
    { // Room 6
        6,
        30, 12,
        0,
        "                      0      0"
        "                      0      0"
        "        OxxOOxxxOxxxxxO      0"
        "        0                    0"
        "        0                    0"
        "        0                    0"
        "        0                    0"
        "        0                    0"
        "        0   OoxxOxxxxxxOxxxOoO"
        "        0   0                 "
        "        0   0                 "
        "        0   0                 ",
        {0, 0, 0, dir_left},
        {0, 0, 0, dir_none},
        room_6
    },
    { // Room 7
        7,
        30, 12,
        0,
        "                      0      0"
        "                      0      0"
        "                      0      0"
        "                      0      0"
        "                      0      0"
        "                      0      0"
        "                      0      0"
        "                      0      0"
        "                      0      0"
        "                      0      0"
        "                      0      0"
        "                      0      0",
        {0, 0, 0, dir_down},
        {0, 0, 0, dir_none},
        room_7
    },
    { // Room 8
        8,
        30, 12,
        0,
        "                      @      @"
        "                      @      @"
        "                      @      @"
        "                      @      @"
        "                      @      @"
        "                      @::::::@"
        "                      @::::::@"
        "                      @      @"
        "                      @      @"
        "                      @      @"
        "                      @      @"
        "                      @      @",
        {0, 0, 0, dir_down},
        {0, 0, 0, dir_none},
        room_8
    },
    { // Room 9
        9,
        30, 12,
        0,
        "                              "
        "                              "
        "                              "
        "                              "
        "                      @@@@@@@@"
        "                      @       "
        "                      @       "
        "                      @       "
        "                      @      @"
        "                      @      @"
        "                      @      @"
        "                      @      @",
        {0, 0, 0, dir_down},
        {0, 0, 0, dir_none},
        room_9
    },
    { // Room 10
        10,
        30, 12,
        0,
        "                              "
        "                              "
        "                              "
        "                              "
        "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
        "                  ::::::      "
        "                  ::::::      "
        "                  ::::::      "
        "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
        "                              "
        "                              "
        "                              ",
        {0, 0, 0, dir_left},
        {0, 0, 0, dir_none},
        room_10
    },
    { // Room 11
        11,
        30, 12,
        0,
        "                              "
        "                              "
        "                              "
        "                              "
        "//////////////////////////////"
        "                              "
        "                              "
        "                              "
        "//////////////////////////////"
        "                              "
        "                              "
        "                              ",
        {0, 0, 0, dir_left},
        {0, 0, 0, dir_none},
        room_11
    },
    { // Room 12 (room 3 trap)
        12,
        50, 25,
        0,
        "##################################################" 
        "#                                                #" 
        "#                                                #" 
        "#                                                #" 
        "#                                                #" 
        "#                                                #" 
        "#                                                #" 
        "#           You found the easter egg!            #" 
        "#               (you can't leave)                #" 
        "#                                                #" 
        "#                  _         _                   #" 
        "#                 (O)       (O)                  #" 
        "#                       o                        #" 
        "#                 _____________                  #" 
        "#                 \\           /                  #" 
        "#                  \\_________/                   #" 
        "#                                                #" 
        "#                                                #" 
        "#                                                #" 
        "#                                                #" 
        "#                                                #" 
        "#                                                #" 
        "#                                                #" 
        "#                                                #" 
        "##################################################",
        {0, 0, 0, dir_none},
        {0, 0, 0, dir_none},
        room_12
    },
    { // Room 13
        13,
        54, 15,
        1,
        "                                                      "
        "                                                      "
        "                                                      "
        "                       You won!                       "
        "                                                      "
        "                                                      "
        "                                                      "
        "                ~Cave Escape Credits~                 "
        "                                                      "
        "      Original:  W. Perez     <wperez274@gmail.com>   "
        "       (http://programswithc.weebly.com/blog/1)       "
        "                                                      "
        "  This version:  Braden Best  <bradentbest@gmail.com> "
        " (https://gitlab.com/bradenbest/cave-escape-improved) "
        "                                                      ",
        {-1, -1, 0, dir_none},
        {-1, -1, 0, dir_none},
        room_13
    },
};

void menu_loop(room *r, player *p){
    int caret_y = 0;
    if (p->last_key == ' '){
        sel_menu = (sel_menu + 1) % 2;
    }

    switch(sel_menu){
        case 0:
            caret_y = 4;
            if(p->last_key == 10){
                p->cur_room = 1;
                p->x = 4;
                p->y = 4;
            }
            break;
        case 1:
            caret_y = 5;
            if(p->last_key == 10){
                end_game(0);
            }
            break;
    }
    mvprintw(caret_y, 10, "[");
    mvprintw(caret_y, 18, "]");
}
void room_1(room *r, player *p){
    if (p->x < 0){
        /* West exit */
        p->cur_room++;
        p->x = r->width - 1;
    } else if (p->x >= r->width){
        /* East exit */
        p->cur_room = 4;
        p->x = 0;
    }

    if (p->y >= r->height - 2){
        /* South wall */
        p->y = r->height - 2;
    } else if (p->y <= 1){
        /* North wall */
        p->y = 1;
    }
}
void room_2(room *r, player *p){
    if(p->x <= 1){
        /* West wall */
        p->x = 1;
    } else if(p->x >= r->width){
        /* East exit */
        p->cur_room--;
        p->x = 0;
    }

    if(p->y <= 1){
        /* North wall */
        p->y = 1;
    } else if(p->y >= r->height && p->x <= 10){
        /* South exit */
        p->cur_room++;
        p->y = 1;
    } else if(p->y >= r->height - 2 && p->x >= 11){
        /* South wall */
        p->y = r->height - 2;
    }
}
void room_3(room *r, player *p){
    if(p->y <= 0 && p->x <= 10){
        /* North exit */
        p->cur_room--;
        p->y = r->height - 1;
    } else if(p->y <= 2 && p->x >= 11){
        /* North wall */
        p->y = 2;
    } else if(p->y >= r->height - 2){
        /* South wall */
        p->y = r->height - 2;
    }

    if(p->x <= 1){
        /* West wall */
        p->x = 1;
    } else if(p->x >= r->width - 1){
        /* East exit (easter egg) */
        p->health = 15000;
        p->cur_room = 12;
        p->x = 25;
        p->y = 10;
    } else if(p->x >= 11){
        /* Waterfall. Forces player down and drowns it. */
        p->direction = dir_down;
        p->y = r->height - 2;
        if(p->shield.active){
            p->health --;
        } else {
            p->health -= 15;
        }
    }
}
void room_4(room *r, player *p){
    if(p->x >= r->width - 2){
        /* East wall */
        p->x = r->width - 2;
    } else if(p->x < 0){
        /* West exit */
        p->cur_room = 1;
        p->x = r->width - 1;
    }
    if(p->y < 0 && p->x > 8 && p->x < 12 ){
        /* North exit */
        p->cur_room++;
        p->y = r->height - 1;
    } else if(p->y <= 1 && (p->x <= 8 || p->x >= 12)){
        /* North wall */
        p->y = 1;
    } else if(p->y >= r->height - 2){
        /* South wall */
        p->y = r->height - 2;
    }
}
void room_5(room *r, player *p){
    if(p->x <= 9){
        /* West wall */
        p->x = 9;
    } else if(p->x >= 11){
        /* East Wall */
        p->x = 11;
    }
    if(p->y < 0){
        /* North exit */
        p->cur_room++;
        p->y = r->height - 1;
    } else if(p->y >= r->height){
        /* South exit */
        p->cur_room--;
        p->y = 0;
    }
}
void room_6(room *r, player *p){
    if(p->direction == dir_left){
        /* West walls */
        if(p->x <= 9 && p->y > 2){
            /* South */
            p->x = 9;
        } else if(p->x <= 23 && p->y <= 2){
            /* North */
            p->x = 23;
        }
    } else if(p->direction == dir_right){
        /* East walls */
        if(p->x >= 11 && p->y > 7){
            /* South */
            p->x = 11;
        } else if(p->x >= r->width - 2 && p->y <= 7){
            /* North */
            p->x = r->width - 2;
        }
    }
    if(p->y >= r->height - 1 && p->x >= 9 && p->x <= 11){
        /* South exit */
        p->cur_room--;
        p->y = 0;
    } else if(p->y >= 7 && p->x > 11){
        /* South wall */
        p->y = 7;
    } else if(p->y < 0){
        /* North exit */
        p->cur_room++;
        p->y = r->height - 1;
    } else if(p->direction == dir_up && p->y <= 3 && p->x >= 9 && p->x <= 22){
        /* North wall */
        p->y = 3;
    }
}
void room_7(room *r, player *p){
    if(p->x <= 23){
        /* West wall */
        p->x = 23;
    } else if(p->x >= r->width - 2){
        /* East wall */
        p->x = r->width - 2;
    }
    if(p->y < 0){
        /* North exit */
        p->cur_room++;
        p->y = r->height - 1;
    } else if(p->y >= r->height){
        /* South exit */
        p->cur_room--;
        p->y = 0;
    }
}
void room_8(room *r, player *p){
    if(p->x <= 23){
        /* West wall */
        p->x = 23;
    } else if(p->x >= r->width - 2){
        /* East wall */
        p->x = r->width - 2;
    }
    if(p->y < 0){
        /* North exit */
        p->cur_room++;
        p->y = r->height - 1;
    } else if(p->y >= r->height){
        /* South exit */
        p->cur_room--;
        p->y = 0;
    }
}
void room_9(room *r, player *p){
    if(p->x <= 23){
        /* West wall */
        p->x = 23;
    } else if(p->x >= r->width - 2 && p->y >= 8){
        /* East wall */
        p->x = r->width - 2;
    } else if(p->x >= r->width){
        /* East exit */
        p->cur_room++;
        p->x = 0;
    }
    if(p->y <= 5){
        /* North wall */
        p->y = 5;
    } else if(p->y >= r->height){
        /* South exit */
        p->cur_room--;
        p->y = 0;
    }
}
void room_10(room *r, player *p){
    if(p->x < 0){
        /* West exit */
        p->cur_room--;
        p->x = r->width - 1;
    } else if(p->x >= r->width){
        /* East exit */
        p->cur_room++;
        p->x = 0;
    }
    if(p->y <= 5){
        /* North wall */
        p->y = 5;
    } else if(p->y >= 7){
        /* South wall */
        p->y = 7;
    }
}
void room_11(room *r, player *p){
    if(p->x < 0){
        /* West exit */
        p->cur_room--;
        p->x = r->width - 1;
    } else if(p->x >= r->width){
        /* East exit */
        p->cur_room = 13;
        p->x = 0;
    }
    if(p->y <= 5){
        /* North wall */
        p->y = 5;
    } else if(p->y >= 7){
        /* South wall */
        p->y = 7;
    }
}
void room_12(room *r, player *p){
    if(p->x >= r->width - 1){
        p->x = 1;
    } else if(p->x <= 0){
        p->x = r->width - 2;
    }
    if(p->y >= r->height - 1){
        p->y = 1;
    } else if(p->y <= 0){
        p->y = r->height - 2;
    }
}
void room_13(room *r, player *p){
    p->x = -1; /* Hide player */
    p->y = -1;
    p->hud_active = 0;
    r->food.x = -2; /* Hide food */
    r->food.y = -2;
    timeout(-1); /* "Pause" game so food can't flicker on-screen briefly */
}
void rooms_entities_init(room *rooms){
    /* Initializes entities (birds and food) for each room */
    int i;
    for(i = 1; i <= num_rooms; i++){
        rooms[i].bird.x = 18 + rand() % 10;
        rooms[i].bird.y =  3 + rand() %  5;
        rooms[i].food.x = 15 + rand() % 10;
        rooms[i].food.y =  5 + rand() %  4;
    }
}
