#ifndef LEVELS_H
#define LEVELS_H

extern room rooms[];

void menu_loop(room *r, player *p);
void room_1(room *r, player *p);
void room_2(room *r, player *p);
void room_3(room *r, player *p);
void room_4(room *r, player *p);
void room_5(room *r, player *p);
void room_6(room *r, player *p);
void room_7(room *r, player *p);
void room_8(room *r, player *p);
void room_9(room *r, player *p);
void room_10(room *r, player *p);
void room_11(room *r, player *p);
void room_12(room *r, player *p);
void room_13(room *r, player *p);
void rooms_entities_init(room *rooms);

#endif
