/* "Cave Escape"
 * Original by wperez274@gmail.com
 *
 * Improved version by Braden Best
 */

#include <ncurses.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "cescape.h"
#include "levels.h"

void bird_logic(entity *bird, player *p){
    int *bs = &bird->state;
    char *sprite;
    switch (bird->direction){
        case dir_up:
            bird->y --;
            break;
        case dir_down:
            bird->y ++;
            break;
        case dir_left:
            bird->x --;
            break;
        case dir_right:
            bird->x ++;
            break;
        default:
            break;
    }
    *bs = ((*bs) + 1) % 10;
    if(*bs < 5){
        sprite = "-^-";
    } else {
        sprite = "-.-";
    }

    if (bird->x < 0){
        bird->x = 20 + rand() % 9;
        bird->y = p->y;
    } else if (bird->x > 29){
        bird->x = rand() % 9;
        bird->y = p->y;
    }

    if (bird->y < 0){
        bird->y = 10;
        bird->x = p->x;
    } else if (bird->y > 10){
        bird->y = 0;
        bird->x = p->x;
    }

    if(bird->direction != dir_none){
        mvprintw(bird->y * Y_SCALE, bird->x * X_SCALE, sprite);
    }
    if(bird->y == p->y && bird->x == p->x && !p->shield.active){
        mvprintw(p->y * Y_SCALE, p->x * X_SCALE, "X");
        p->health -= 10;
    }
}

void end_game(int exit_code){
    int i;
    erase();
    endwin();
    if(exit_code){
        for(i = 0; i < 100; i++){
            putchar('\n');
        }
        printf("Game Over.\n");
    }
    exit(0);
}

void food_logic(entity *food, player *p){
    int *fs = &food->state;
    char *sprite;
    if (rand() % 100 < 3){
        food->x = rand() % 29 + 1;
        food->y = 1+rand() % 10 + 1;
        if(!p->shield.active){
            p->health -= 5;
        } else {
            p->health += 1;
        }
    }
    *fs = ((*fs) + 1) % 10;
    if(*fs < 5){
        sprite = "e";
    } else {
        sprite = ".";
    }

    if (p->x == food->x && p->y == food->y){
        food->x = rand() % 29 + 1;
        food->y = 1+rand() % 10 + 1;
        p->health += 15;
        p->shield.energy++;
        if(p->shield.active){
            p->shield.age -= 20;
        }
    }
    mvprintw(food->y * Y_SCALE, food->x * X_SCALE, sprite);
}

void game_loop(){
    player p = {
        0, 6, 4, 75, 0, 0, 1,
        dir_right,
        { 0, 100, 0, 0 }
    };
    entity *sel_bird;
    entity *sel_food;
    room *sel_room;

    rooms_entities_init(rooms);

    while(1){
        p.last_key = getch();
        sel_room = &rooms[p.cur_room];
        sel_bird = &sel_room->bird;
        sel_food = &sel_room->food;

        if(!p.cur_room){
            // Don't let anything else happen while room 0 (menu) is active
            room_draw(sel_room);
            sel_room->rules(sel_room, &p);
            continue;
        } else {
            // during game, logic first, draw later
            player_do_keys(&p);
            if(p.paused){
                continue;
            }
            player_move(&p);
            sel_room->rules(sel_room, &p);
            room_draw(sel_room);
            player_draw(sel_room, &p);
        }
        bird_logic(sel_bird, &p);
        food_logic(sel_food, &p);
    }
}

void player_do_keys(player *p){
    if(!p->paused){
        switch (p->last_key){
            case KEY_LEFT:
            case 'h':
                p->direction = dir_left;
                break;
            case KEY_RIGHT:
            case 'l':
                p->direction = dir_right;
                break;
            case KEY_UP:
            case 'k':
                p->direction = dir_up;
                break;
            case KEY_DOWN:
            case 'j':
                p->direction = dir_down;
                break;
            case 'q':
                end_game(0);
            case 'p':
                timeout(-1);
                p->paused = 1;
                break;
        }
    } else if(p->last_key == 'p'){
        p->paused = 0;
        timeout(GAME_SPEED);
    }
}

void player_draw(room *r, player *p){
    char player_sprite[4] = "";
    char *health_bar = player_health_bar(p);
    char *shield_bar = player_shield_bar(p);

    if (p->health < 1){
        end_game(1);
    }

    if (p->shield.energy >= 3){
        p->shield.active = 1;
    }

    if (p->shield.active){
        p->shield.age++;
        strcpy(player_sprite, "(O)");
    } else if (p->health >= 50){
        strcpy(player_sprite, " O ");
    } else if (p->health < 50){
        strcpy(player_sprite, " o ");
    }
    mvprintw(p->y * Y_SCALE, p->x * X_SCALE - 1, player_sprite);
    if(r->id == 12){
        mvprintw(p->y * Y_SCALE - 1, p->x * X_SCALE, "^ <-- Dunce");
    }

    if(p->hud_active){
        mvprintw(r->height * Y_SCALE + 1, 0, "Health: [%s] %u", health_bar, p->health);
        mvprintw(r->height * Y_SCALE + 2, 0, "Shield: %s", shield_bar);
    }
    free(health_bar);
    free(shield_bar);

    if (p->shield.age >= p->shield.duration){
        p->shield.age = 0;
        p->shield.active = 0;
        p->shield.energy = 0;
    }
}

char *player_health_bar(player *p){
    char *ret = malloc(sizeof(char) * 21);
    char *r = ret;
    int i = 0;
    int health = p->health;
    while(i++ < 20){
        if(health > 0){
            *(r++) = p->shield.active ? '#' : '=';
        } else {
            *(r++) = ' ';
        }
        health -= 5;
    }
    *r = 0;
    return ret;
}

void player_move(player *p){
    switch (p->direction){
        case dir_up:
            p->y --;
            break;
        case dir_down:
            p->y ++;
            break;
        case dir_left:
            p->x --;
            break;
        case dir_right:
            p->x ++;
            break;
        default:
            break;
    }
}

char *player_shield_bar(player *p){
    char *ret = malloc(sizeof(char) * 8);
    char *r = ret + 5;
    int senergy = p->shield.energy;
    int shield_timeout = (p->shield.duration - p->shield.age) / 10;
    switch(senergy){
        case 0:
            strcpy(ret, "- - -");
            break;
        case 1:
            strcpy(ret, "* - -");
            break;
        case 2:
            strcpy(ret, "* * -");
            break;
        default:
            strcpy(ret, "*****");
            break;
    }
    if(p->shield.active){
        *(r++) = ' ';
        *r = '0' + shield_timeout;
    }
    return ret;
}

void player_turn_around(player *p){
    switch(p->direction){
        case dir_up:
            p->direction = dir_down;
            break;
        case dir_down:
            p->direction = dir_up;
            break;
        case dir_left:
            p->direction = dir_right;
            break;
        case dir_right:
            p->direction = dir_left;
            break;
        default:
            p->direction = dir_right;
            break;
    }
}

void room_draw(room *r){
    int i, j,
        xs = X_SCALE,
        ys = Y_SCALE;
    erase();
    if(r->force_1by1){
        xs = 1;
        ys = 1;
    }
    for(i = 0; i < r->height; i++){
        for(j = 0; j < r->width; j++){
            mvaddch(i * ys + 0, j * xs + 0, r->map[j + (i * r->width)]);
        }
    }
}

int main(){
    srand(time(0));
    initscr();
    noecho();
    curs_set(0);
    keypad(stdscr, TRUE);
    timeout(GAME_SPEED);  
    start_color();

    bkgd(COLOR_PAIR(1));
    init_pair(1, COLOR_CYAN, COLOR_BLACK);

    game_loop();
    end_game(0);
}
