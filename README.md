# Cave Escape Improved
### A complete rewrite of Wperez's [Cave Escape](http://programswithc.weebly.com/blog/1)

Well, techincally it's a refactoring, but it's so extensive that I might as well have rewritten it.

## Usage

    $ cd src && make  # to compile
    $ ./cave-escape   # to run
